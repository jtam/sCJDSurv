"""scikit-learn compatible ensemble model"""

import json
import random

import numpy as np
import pandas as pd
import torch
import torchtuples as tt
from lifelines.utils import median_survival_times
from pycox.models import MTLR
from sklearn.preprocessing import StandardScaler
from sklearn.utils.validation import check_is_fitted
from sksurv.linear_model import CoxPHSurvivalAnalysis, CoxnetSurvivalAnalysis
from sksurv.metrics import integrated_brier_score

from .nn import model_dict
from .preprocess import CatEncode
from joblib import Parallel, delayed
from tqdm.auto import trange
from sksurv.datasets import get_x_y
from .config import BATCH_SIZE, SEED


def load_parameters(path:str) -> pd.Series:
    """Load parameters from a given JSON file.

    Args:
        path (str): Best parameters stored as JSON.

    Raises:
        ValueError: Validate that parameters for all models are present.

    Returns:
        pd.Series: Best parameters
    """
    
    # Load JSON
    with open(path) as file:
        parameters = json.load(file)

    # Organise into pandas.Series
    parameters = pd.DataFrame(parameters).set_index('imputed_dataset').sort_index()['best_params']
    
    # Validate
    if not parameters.index.to_list() == list(range(10)):
        raise ValueError('JSON length is not equal to number of imputed datasets (10).')
    else:
        return parameters

class sCJDSurvModel:
    
    def __init__(self, model_name:str, device='cpu'):
        """Instantiate a single model in the sCJDSurv ensemble

        Args:
            model_name (str): Official model name
            device (str, optional): Device to train on. Defaults to 'cpu'.
        """
        self._estimator_type = 'regressor'
        self.model_name = model_name
        self.device = device
    
    def fit(self, params:dict, X:pd.DataFrame, y:np.array):
        """Fit a single sCJDSurv model

        Args:
            params (dict): Model parameters
            X (pd.DataFrame): Feature DataFrame
            y (np.array): Label array

        Returns:
            self: trained model
        """
        
        # Store given params in object
        self.params = params
        
        # Extract parameters
        lr = params['lr']
        try:
            batch_size = params['batch_size']
        except:
            batch_size = BATCH_SIZE
        epochs = params['epochs']
        weight_decay = params['weight_decay']
        
        net_params = {}
            
        try:
            net_params.update({'dropout': params['dropout']})
        except:
            pass
        try:
            net_params.update({'hl1_nodes': params['hl1_nodes']})
        except:
            pass
        try:
            net_params.update({'hl2_nodes': params['hl2_nodes']})
        except:
            pass
        try:
            net_params.update({'hl3_nodes': params['hl3_nodes']})
        except:
            pass
        try:
            net_params.update({'hl4_nodes': params['hl4_nodes']})
        except:
            pass
        try:
            net_params.update({'hl5_nodes': params['hl5_nodes']})
        except:
            pass
        
        # Set random seeds
        torch.manual_seed(SEED)
        np.random.seed(SEED)
        random.seed(SEED)
        
        #### PREPROCESSING ####
        
        # CatEncode X
        self.encoder = CatEncode(drop_dummy=True)
        self.encoder.fit(X)
        X = self.encoder.transform(X)
        
        # Standardize X
        self.ss = StandardScaler()
        X = self.ss.fit_transform(X).astype('float32')
        
        # Label Transforms
        num_durations = 100
        labtrans = MTLR.label_transform(num_durations, 'equidistant')
        get_target = lambda df: (df['duration_m'], df['status'])
        y_pycox = labtrans.fit_transform(*get_target(y))
        
        # Create neural net
        net = model_dict[self.model_name](
            X.shape[1],
            out_features=labtrans.out_features,
            **net_params
        )
        
        # Set optimizier
        optimizer = tt.optim.Adam(lr, weight_decay=weight_decay)
        
        # Set model
        self.model = MTLR(net, optimizer, duration_index=labtrans.cuts, device=self.device)
        
        
        # Train model
        log = self.model.fit(
            X,
            y_pycox,
            batch_size=batch_size,
            epochs=epochs,
            verbose=False,
        )
        
        # Set fitted data
        self.X_ = X
        self.y_ = y
        
        return self
    
    def predict_surv_df(self, X:pd.DataFrame, sub:int=10) -> pd.DataFrame:
        """Predict survival function

        Args:
            X (pd.DataFrame): Feature DataFrame
            sub (int, optional): Number of 'sub' units in interpolation grid. Defaults to 10.

        Returns:
            pd.DataFrame: Predicted survival functions (n_timepoints, n_individuals)
        """
        
        # Check if fit has been called
        check_is_fitted(self)

        # CatEncode X
        X = self.encoder.transform(X)
        
        # Standardize X
        X = self.ss.transform(X).astype('float32')
        
        surv = self.model.interpolate(sub).predict_surv_df(X)
        
        return surv
    
    def predict(self, X:pd.DataFrame, sub=10) -> np.array:
        """Predict risk scores

        Args:
            X (pd.DataFrame): Feature DataFrame
            sub (int, optional): Number of 'sub' units in interpolation grid. Defaults to 10.

        Returns:
            np.array: Predicted risk scores (n_individuals)
        """
        
        surv = self.predict_surv_df(X, sub=sub)
        return 1-surv.mean(axis=0)
    
    def score(self, X, y):
        surv = self.predict_surv_df(X).T
        model_times = surv.columns
        
        # Find indices of event times of model that correspond to test times
        indices = np.where(
            np.logical_and(
                model_times > min(y['duration_m']),
                model_times < max(y['duration_m'])
            )
        )
        
        # Set estimator time window to all available timepoints
        times = model_times[indices]
        
        # Subset surv to times
        surv = surv[times]
        
        return -integrated_brier_score(
            self.y_,
            y,
            surv,
            times
        )

class sCJDSurvEnsemble:
    
    def __init__(self, model_name:str, device='cpu'):
        """Instantiate a sCJDSurv ensemble

        Args:
            model_name (str): Official model name
            device (str, optional): Device to train on. Defaults to 'cpu'.
        """
        self._estimator_type = 'regressor'
        self.model_name = model_name
        self.device = device
        
    def fit(self, imputed_train:list, parameters:dict, n_jobs:int=1, progress_bar=True):
        """Fit ensemble model to imputed training data

        Args:
            imputed_train (list): Imputed training data
            parameters (dict): Parameters for sCJDSurvModel
            n_jobs (int, optional): Number of workers. >2 may cause instability. Defaults to 1.
            progress_bar (bool, optional): Show tqdm progress bar. Defaults to True.
        """
        
        def _fit_single_model(mice_i):
            params = parameters.loc[mice_i]
            X_train, y_train = get_x_y(imputed_train[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
            model = sCJDSurvModel(self.model_name)
            model.fit(params, X_train, y_train)
            return {mice_i: model}
        
        # Train models independently
        self.ensemble = Parallel(n_jobs)(delayed(_fit_single_model)(mice_i) for mice_i in trange(10, disable=not progress_bar))
        
        # Form flat dictionary of models
        self.ensemble = {k: v for d in self.ensemble for k, v in d.items()}
        
        return self
    
    def predict_surv_df(
        self, 
        X:pd.DataFrame=None,
        imputed_test:list=None,
        pool=False,
        n_jobs=1,
        sub=10
    ):
        """Predict survival function using either a single feature DataFrame or a list of feature DataFrames (imputed_test)

        Args:
            X (pd.DataFrame, optional): Single feature DataFrame. Defaults to None.
            imputed_test (list, optional): List of imputed DataFrames. Defaults to None.
            pool (bool, optional): Pool results. Defaults to False.
            n_jobs (int, optional): Number of workers for joblib multiprocessing. Defaults to 1.
            sub (int, optional): Number of 'sub' units in interpolation grid. Defaults to 10.

        Raises:
            ValueError: if both X and imputed_test are provided

        Returns:
            list or pd.DataFrame: list of pd.DataFrame if pool=False, pd.DataFrame if pool=True
        """
        
        # Validate input
        if (X is not None) and (imputed_test is not None) :
            raise ValueError('Both X and imputed_test provided')
        
        def _single_model_predict_surv_df(item, X=None, imputed_test=None):
            
            # Unpack self.ensemble
            mice_i, model = item[0], item[1]
            
            # If X provided
            if X is not None:
                return model.predict_surv_df(X, sub)
            
            # If imputed_test provided
            elif imputed_test is not None:
                X_test, y_test = get_x_y(imputed_test[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
                return model.predict_surv_df(X_test, sub)
        
        # Predict
        results = Parallel(n_jobs)(delayed(_single_model_predict_surv_df)(item, X, imputed_test) for item in self.ensemble.items())
        
        # Pooling
        if pool:
            results = pd.concat(results)
            results = results.groupby(results.index).mean()
        
        return results
    
    def predict(self, X:pd.DataFrame=None, imputed_test:list=None, pool=False, sub=10):
        """Predict risk scores using either a single feature DataFrame or a list of feature DataFrames (imputed_test)

        Args:
            X (pd.DataFrame, optional): Single feature DataFrame. Defaults to None.
            imputed_test (list, optional): List of imputed DataFrames. Defaults to None.
            pool (bool, optional): Pool results. Defaults to False.
            sub (int, optional): Number of 'sub' units in interpolation grid. Defaults to 10.

        Raises:
            ValueError: if both X and imputed_test are provided

        Returns:
            list of np.array or pd.Series: list of np.array if pool=False, pd.Series if pool=True
        """
        
        results = []
        
        # Validate input
        if (X is not None) and (imputed_test is not None) :
            raise ValueError('Both X and imputed_test provided')
        # If X provided
        elif X is not None:
            for item in self.ensemble.items():
                model = item[1]
                results.append(model.predict(X, sub=sub))
        # If imputed_test provided
        elif imputed_test is not None:
            for item in self.ensemble.items():
                mice_i, model = item[0], item[1]
                X_test, y_test = get_x_y(imputed_test[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
                results.append(model.predict(X_test, sub=sub))
        
        if pool:
            results = pd.DataFrame(results).mean()
        
        return results

class CoxPHEnsemble:
    
    def __init__(self):
        """Instantiate a CoxPHEnsemble ensemble"""
        
        self._estimator_type = 'regressor'
        
    def fit(self, imputed_train:list, n_jobs:int=1, progress_bar=True):
        """Fit ensemble model to imputed training data

        Args:
            imputed_train (list): Imputed training data
            n_jobs (int, optional): Number of workers. >2 may cause instability. Defaults to 1.
            progress_bar (bool, optional): Show tqdm progress bar. Defaults to True.
        """
        
        def _fit_single_model(mice_i):
            
            # Form X and y arrays
            X, y = get_x_y(imputed_train[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
            
            # CatEncode X
            encoder = CatEncode(drop_dummy=True)
            encoder.fit(X)
            X = encoder.transform(X)
            
            model = CoxPHSurvivalAnalysis()
            model.fit(X, y)
            return {mice_i: {'model': model, 'encoder': encoder}}
        
        # Train models independently
        self.ensemble = Parallel(n_jobs)(delayed(_fit_single_model)(mice_i) for mice_i in trange(10, disable=not progress_bar))
        
        # Form flat dictionary of models
        self.ensemble = {k: v for d in self.ensemble for k, v in d.items()}
        
        return self
    
    def predict_surv_df(
        self, 
        X:pd.DataFrame=None,
        imputed_test:list=None,
        pool=False,
        n_jobs=1,
        sub=10
    ):
        """Predict survival function using either a single feature DataFrame or a list of feature DataFrames (imputed_test)

        Args:
            X (pd.DataFrame, optional): Single feature DataFrame. Defaults to None.
            imputed_test (list, optional): List of imputed DataFrames. Defaults to None.
            pool (bool, optional): Pool results. Defaults to False.
            n_jobs (int, optional): Number of workers for joblib multiprocessing. Defaults to 1.
            sub (int, optional): Number of 'sub' units in interpolation grid. Defaults to 10.

        Raises:
            ValueError: if both X and imputed_test are provided

        Returns:
            list or pd.DataFrame: list of pd.DataFrame if pool=False, pd.DataFrame if pool=True
        """
        
        # Validate input
        if (X is not None) and (imputed_test is not None) :
            raise ValueError('Both X and imputed_test provided')
        
        def _single_model_predict_surv_df(item, X=None, imputed_test=None):
            
            # Unpack self.ensemble
            mice_i, model, encoder = item[0], item[1]['model'], item[1]['encoder']
            
            # Unpack model_times
            model_times = model.unique_times_
            
            # If X provided
            if X is not None:
                X = encoder.transform(X)
                surv = model.predict_survival_function(X, return_array=True)
                return pd.DataFrame(surv, columns=model_times).T
            
            # If imputed_test provided
            elif imputed_test is not None:
                X_test, y_test = get_x_y(imputed_test[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
                X_test = encoder.transform(X_test)
                surv = model.predict_survival_function(X_test, return_array=True)
                return pd.DataFrame(surv, columns=model_times).T
        
        # Predict
        results = Parallel(n_jobs)(delayed(_single_model_predict_surv_df)(item, X, imputed_test) for item in self.ensemble.items())
        
        # Pooling
        if pool:
            results = pd.concat(results)
            results = results.groupby(results.index).mean()
        
        return results

    def predict(self, X:pd.DataFrame=None, imputed_test:list=None, pool=False):
        """Predict risk scores using either a single feature DataFrame or a list of feature DataFrames (imputed_test)

        Args:
            X (pd.DataFrame, optional): Single feature DataFrame. Defaults to None.
            imputed_test (list, optional): List of imputed DataFrames. Defaults to None.
            pool (bool, optional): Pool results. Defaults to False.

        Raises:
            ValueError: if both X and imputed_test are provided

        Returns:
            list of np.array or pd.Series: list of np.array if pool=False, pd.Series if pool=True
        """
        
        results = []
        
        # Validate input
        if (X is not None) and (imputed_test is not None) :
            raise ValueError('Both X and imputed_test provided')
        # If X provided
        elif X is not None:
            surv_dfs = self.predict_surv_df(X=X)
            results = [1-surv_df.mean(axis=0) for surv_df in surv_dfs]
        # If imputed_test provided
        elif imputed_test is not None:
            surv_dfs = self.predict_surv_df(imputed_test=imputed_test)
            results = [1-surv_df.mean(axis=0) for surv_df in surv_dfs]
        
        if pool:
            results = pd.DataFrame(results).mean()
        
        return results
        

class CoxLASSOEnsemble:
    
    def __init__(self):
        """Instantiate a CoxPHEnsemble ensemble"""
        
        self._estimator_type = 'regressor'
        
    def fit(self, imputed_train:list, alpha:float, n_jobs:int=1, progress_bar=True):
        """Fit ensemble model to imputed training data

        Args:
            imputed_train (list): Imputed training data
            n_jobs (int, optional): Number of workers. >2 may cause instability. Defaults to 1.
            progress_bar (bool, optional): Show tqdm progress bar. Defaults to True.
        """
        
        def _fit_single_model(mice_i, alpha):
            
            # Form X and y arrays
            X, y = get_x_y(imputed_train[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
            
            # CatEncode X
            encoder = CatEncode(drop_dummy=True)
            encoder.fit(X)
            X = encoder.transform(X)
            
            model = CoxnetSurvivalAnalysis(alphas=[alpha], l1_ratio=1.0, fit_baseline_model=True)
            model.fit(X, y)
            return {mice_i: {'model': model, 'encoder': encoder}}
        
        # Train models independently
        self.ensemble = Parallel(n_jobs)(delayed(_fit_single_model)(mice_i, alpha) for mice_i in trange(10, disable=not progress_bar))
        
        # Form flat dictionary of models
        self.ensemble = {k: v for d in self.ensemble for k, v in d.items()}
        
        return self
    
    def predict_surv_df(
        self, 
        X:pd.DataFrame=None,
        imputed_test:list=None,
        pool=False,
        n_jobs=1,
        sub=10
    ):
        """Predict survival function using either a single feature DataFrame or a list of feature DataFrames (imputed_test)

        Args:
            X (pd.DataFrame, optional): Single feature DataFrame. Defaults to None.
            imputed_test (list, optional): List of imputed DataFrames. Defaults to None.
            pool (bool, optional): Pool results. Defaults to False.
            n_jobs (int, optional): Number of workers for joblib multiprocessing. Defaults to 1.
            sub (int, optional): Number of 'sub' units in interpolation grid. Defaults to 10.

        Raises:
            ValueError: if both X and imputed_test are provided

        Returns:
            list or pd.DataFrame: list of pd.DataFrame if pool=False, pd.DataFrame if pool=True
        """
        
        # Validate input
        if (X is not None) and (imputed_test is not None) :
            raise ValueError('Both X and imputed_test provided')
        
        def _single_model_predict_surv_df(item, X=None, imputed_test=None):
            
            # Unpack self.ensemble
            mice_i, model, encoder = item[0], item[1]['model'], item[1]['encoder']
            
            # Unpack model_times
            model_times = model.unique_times_
            
            # If X provided
            if X is not None:
                X = encoder.transform(X)
                surv = model.predict_survival_function(X, return_array=True)
                return pd.DataFrame(surv, columns=model_times).T
            
            # If imputed_test provided
            elif imputed_test is not None:
                X_test, y_test = get_x_y(imputed_test[mice_i], attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
                X_test = encoder.transform(X_test)
                surv = model.predict_survival_function(X_test, return_array=True)
                return pd.DataFrame(surv, columns=model_times).T
        
        # Predict
        results = Parallel(n_jobs)(delayed(_single_model_predict_surv_df)(item, X, imputed_test) for item in self.ensemble.items())
        
        # Pooling
        if pool:
            results = pd.concat(results)
            results = results.groupby(results.index).mean()
        
        return results
    
    def predict(self, X:pd.DataFrame=None, imputed_test:list=None, pool=False):
        """Predict risk scores using either a single feature DataFrame or a list of feature DataFrames (imputed_test)

        Args:
            X (pd.DataFrame, optional): Single feature DataFrame. Defaults to None.
            imputed_test (list, optional): List of imputed DataFrames. Defaults to None.
            pool (bool, optional): Pool results. Defaults to False.

        Raises:
            ValueError: if both X and imputed_test are provided

        Returns:
            list of np.array or pd.Series: list of np.array if pool=False, pd.Series if pool=True
        """
        
        results = []
        
        # Validate input
        if (X is not None) and (imputed_test is not None) :
            raise ValueError('Both X and imputed_test provided')
        # If X provided
        elif X is not None:
            surv_dfs = self.predict_surv_df(X=X)
            results = [1-surv_df.mean(axis=0) for surv_df in surv_dfs]
        # If imputed_test provided
        elif imputed_test is not None:
            surv_dfs = self.predict_surv_df(imputed_test=imputed_test)
            results = [1-surv_df.mean(axis=0) for surv_df in surv_dfs]
        
        if pool:
            results = pd.DataFrame(results).mean()
        
        return results