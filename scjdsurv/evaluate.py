from sksurv.metrics import (brier_score, concordance_index_censored,
                            cumulative_dynamic_auc, integrated_brier_score)
from sklearn.metrics import roc_curve
from sksurv.datasets import get_x_y
import numpy as np
import pandas as pd
from tabulate import tabulate

def c_index(surv, y_test):
    surv = surv.reset_index(drop=True)
    risk_scores = 1-surv.mean(axis=0)
    
    return concordance_index_censored(
        y_test['status'].astype(bool),
        y_test['duration_m'],
        risk_scores
    )[0]

def ibs(surv, y_train, y_test):
            
    surv = surv.T
    model_times = surv.columns
    
    # Find indices of event times of model that correspond to test times
    indices = np.where(
        np.logical_and(
            model_times > min(y_test['duration_m']),
            model_times < max(y_test['duration_m'])
        )
    )
    
    # Set estimator time window to all available timepoints
    times = model_times[indices]
    
    # Subset surv to times
    surv = surv[times]
    
    return integrated_brier_score(
        y_train,
        y_test,
        surv,
        times
    )
    
def brier_df(surv, y_train, y_test):

    surv = surv.T
    model_times = surv.columns
    
    # Find indices of event times of model that correspond to test times
    indices = np.where(
        np.logical_and(
            model_times > min(y_test['duration_m']),
            model_times < max(y_test['duration_m'])
        )
    )
    
    # Set estimator time window to all available timepoints
    times = model_times[indices]
    
    # Subset surv to times
    surv = surv[times]
    
    brier = brier_score(
        y_train,
        y_test,
        surv,
        times
    )
    
    return pd.DataFrame(brier[1], index=brier[0], columns=['brier'])

def tdauc(surv, y_train, y_test, months):
    
    surv_df = surv.T
    model_times = surv_df.columns
    risk_df = 1 - surv_df

    # Find indices of event times of model that correspond to test times
    indices = np.where(
        np.logical_and(
            model_times > min(y_test['duration_m']),
            model_times < max(y_test['duration_m'])
        )
    )
    
    # Set times and risk_df to all available timepoints
    times = model_times[indices]
    risk_df = risk_df[times]

    auc =  cumulative_dynamic_auc(
        y_train,
        y_test,
        risk_df,
        times
    )[0]
    
    auc_df = pd.DataFrame(auc, index=times, columns=['auc'])
    
    auc_t = []
    
    for month in months:
        nearest_month = min(auc_df.index, key=lambda x:abs(x-month))
        auc_t.append(auc_df.auc[auc_df.index == nearest_month].values[0])
    
    return auc_t, auc_df

def roc_stats(surv, y_test, month):

    surv_df = surv.T
    model_times = surv_df.columns
    risk_df = 1 - surv_df

    month = min(model_times, key=lambda x:abs(x-month))

    y_true = y_test['duration_m'] <= month
    y_score = risk_df[month]

    fpr, tpr, _ = roc_curve(y_true, y_score)

    return {'fpr': fpr, 'tpr': tpr}

def evaluate_ensemble(
    pred:list,
    imputed_train:list,
    imputed_test:list,
    months=[5,10],
    pool=True,
    print_results=True
):
    
    scores = {}

    for mice_i in range(10):

        train = imputed_train[mice_i]
        test = imputed_test[mice_i]
        surv = pred[mice_i]
        X_train, y_train = get_x_y(train, attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
        X_test, y_test = get_x_y(test, attr_labels=['status', 'duration_m'], pos_label=True, survival=True)

        auc_t, auc_df = tdauc(surv, y_train, y_test, months)

        scores.update(
            {
                mice_i: {
                    'c_index': c_index(surv, y_test),
                    'ibs': ibs(surv, y_train, y_test),
                    'brier_df': brier_df(surv, y_train, y_test),
                    f'auc_{months[0]}': auc_t[0],
                    f'auc_{months[1]}': auc_t[1],
                    'auc_df': auc_df
                }
            }
        )
    
    if not pool:
        return scores
    else:
        mean_c_index = np.mean([score['c_index'] for score in scores.values()])
        mean_ibs = np.mean([score['ibs'] for score in scores.values()])
        mean_auc_5 = np.mean([score['auc_5'] for score in scores.values()])
        mean_auc_10 = np.mean([score['auc_10'] for score in scores.values()])
        mean_brier_df = pd.concat([score['brier_df'] for score in scores.values()], axis=1, join='outer').mean(axis=1)
        mean_auc_df = pd.concat([score['auc_df'] for score in scores.values()], axis=1, join='outer').mean(axis=1)

        # Package results
        results = {
            'c_index': mean_c_index,
            'ibs': mean_ibs,
            'auc_5': mean_auc_5,
            'auc_10': mean_auc_10,
            'brier_df': mean_brier_df,
            'auc_df': mean_auc_df
        }
        
        if print_results:
            table = [
                ['Concordance index', "%.3f" % mean_c_index],
                ['Integrated Brier score', "%.3f" % mean_ibs],
                ['AUC at 5 months', "%.3f" % mean_auc_5],
                ['AUC at 10 months', "%.3f" % mean_auc_10]
            ]
            
            print(tabulate(table, tablefmt='plain'))
        
        return results
