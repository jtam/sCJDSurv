import torch.nn as nn

__all__ = [
    'LinearMTLR',
    'NMTLR2',
    'NMTLR3',
    'NMTLR4',
    'NMTLR5',
    'NMTLR6',
    'NMTLR7',
    'model_dict'
]

class LinearMTLR(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features
    ):
        
        super().__init__()
        
        self.model_name = 'linear model'
        self.net = nn.Linear(in_features, out_features, bias=False)
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x
 
    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()

class NMTLR2(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features,
        **kwargs
    ):
        
        super().__init__()
        
        self.model_name = '2 layer MLP, ReLU, BatchNorm, Dropout'
        
        hl1_nodes = kwargs['hl1_nodes']
        dropout = kwargs['dropout']
        
        self.net = nn.Sequential(
            
            nn.Linear(in_features, hl1_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl1_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl1_nodes, out_features, bias=False)
        )
        
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x
    
    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()

class NMTLR3(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features,
        **kwargs
    ):
        
        super().__init__()
        
        self.model_name = '3 layer MLP, ReLU, BatchNorm, Dropout'

        hl1_nodes = kwargs['hl1_nodes']
        hl2_nodes = kwargs['hl2_nodes']
        dropout = kwargs['dropout']
        
        self.net = nn.Sequential(
            
            nn.Linear(in_features, hl1_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl1_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl1_nodes, hl2_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl2_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl2_nodes, out_features, bias=False)
        )
        
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x
    
    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()

class NMTLR4(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features,
        **kwargs
    ):
        
        super().__init__()
        
        self.model_name = '4 layer MLP, ReLU, BatchNorm, Dropout'
        
        hl1_nodes = kwargs['hl1_nodes']
        hl2_nodes = kwargs['hl2_nodes']
        hl3_nodes = kwargs['hl3_nodes']
        dropout = kwargs['dropout']
        
        self.net = nn.Sequential(
            
            nn.Linear(in_features, hl1_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl1_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl1_nodes, hl2_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl2_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl2_nodes, hl3_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl3_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl3_nodes, out_features, bias=False)
        )
        
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x

    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()

class NMTLR5(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features,
        **kwargs
    ):
        
        super().__init__()
        
        self.model_name = '5 layer MLP, ReLU, BatchNorm, Dropout'
        
        hl1_nodes = kwargs['hl1_nodes']
        hl2_nodes = kwargs['hl2_nodes']
        hl3_nodes = kwargs['hl3_nodes']
        hl4_nodes = kwargs['hl4_nodes']
        dropout = kwargs['dropout']
        
        self.net = nn.Sequential(
            
            nn.Linear(in_features, hl1_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl1_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl1_nodes, hl2_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl2_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl2_nodes, hl3_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl3_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl3_nodes, hl4_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl4_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl4_nodes, out_features, bias=False)
        )
        
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x
    
    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()
    
class NMTLR6(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features,
        **kwargs
    ):
        
        super().__init__()
        
        self.model_name = '6 layer MLP, ReLU, BatchNorm, Dropout'
        
        hl1_nodes = kwargs['hl1_nodes']
        hl2_nodes = kwargs['hl2_nodes']
        hl3_nodes = kwargs['hl3_nodes']
        hl4_nodes = kwargs['hl4_nodes']
        hl5_nodes = kwargs['hl5_nodes']
        dropout = kwargs['dropout']
        
        self.net = nn.Sequential(
            
            nn.Linear(in_features, hl1_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl1_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl1_nodes, hl2_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl2_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl2_nodes, hl3_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl3_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl3_nodes, hl4_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl4_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl4_nodes, hl5_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl5_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl5_nodes, out_features, bias=False)
        )
        
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x
    
    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()

class NMTLR7(nn.Module):
    
    def __init__(
        self, 
        in_features,
        out_features,
        **kwargs
    ):
        
        super().__init__()
        
        self.model_name = '7 layer MLP, ReLU, BatchNorm, Dropout'
        
        hl1_nodes = kwargs['hl1_nodes']
        hl2_nodes = kwargs['hl2_nodes']
        hl3_nodes = kwargs['hl3_nodes']
        hl4_nodes = kwargs['hl4_nodes']
        hl5_nodes = kwargs['hl5_nodes']
        hl6_nodes = kwargs['hl6_nodes']
        dropout = kwargs['dropout']
        
        self.net = nn.Sequential(
            
            nn.Linear(in_features, hl1_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl1_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl1_nodes, hl2_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl2_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl2_nodes, hl3_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl3_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl3_nodes, hl4_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl4_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl4_nodes, hl5_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl5_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl5_nodes, hl6_nodes),
            nn.ReLU(),
            nn.BatchNorm1d(hl6_nodes),
            nn.Dropout(dropout),
            
            nn.Linear(hl6_nodes, out_features, bias=False)
        )
        
        self.apply(self._init_weights)

    def forward(self, x):
        x = self.net(x)
        return x
    
    def _init_weights(self, module):
        if isinstance(module, nn.Linear):
            nn.init.kaiming_normal_(module.weight, mode='fan_in', nonlinearity='relu')
            if module.bias is not None:
                module.bias.data.zero_()

model_dict = {
    'linear': LinearMTLR,
    '2 layers MLP, ReLU, BatchNorm, Dropout': NMTLR2,
    '3 layers MLP, ReLU, BatchNorm, Dropout': NMTLR3,
    '4 layers MLP, ReLU, BatchNorm, Dropout': NMTLR4,
    '5 layers MLP, ReLU, BatchNorm, Dropout': NMTLR5,
    '6 layers MLP, ReLU, BatchNorm, Dropout': NMTLR6,
    '7 layers MLP, ReLU, BatchNorm, Dropout': NMTLR7
}