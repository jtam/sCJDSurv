import json
import random
import warnings
from os import mkdir

import numpy as np
import optuna
import pandas as pd
import torch
import torchtuples as tt
from optuna.exceptions import ExperimentalWarning
from pycox.models import MTLR
from sklearn.model_selection import KFold
from sklearn.preprocessing import StandardScaler
from sksurv.datasets import get_x_y
from sksurv.metrics import integrated_brier_score
from sqlalchemy_utils import create_database, database_exists, drop_database

from .nn import *
from .preprocess import CatEncode
from .config import BATCH_SIZE, SEED

__all__ = [
    'objective',
    'OptunaSearch'
]

def objective(
    trial: optuna.trial.Trial,
    train: pd.DataFrame,
    model_name: str,
    device='cpu',
):
    """Optuna objective function for hyperparameter search by cross validation

    Args:
        trial (optuna.trial.Trial): Optuna trial object
        train (pd.DataFrame): Training set
        model_name (str): Chosen model name (see model_dict from cjdsurv.models)
        device (str, optional): Train on 'cpu' or 'cuda. Defaults to 'cpu'.
        n_jobs (int, optional): If training with CPU, n_jobs for joblib parallelisation. Defaults to 1.

    Raises:
        optuna.TrialPruned: Optuna pruning

    Returns:
        loss: trial loss
    """
    
    # Set parameters to fix
    batch_size = trial.suggest_categorical('batch_size', [16,32,64,128,256,512,1024])
    
    # Set parameters to tune
    lr = trial.suggest_float('lr', 0 , 1, step=1e-3)
    epochs = trial.suggest_int('epochs', 10, 200)
    weight_decay = trial.suggest_float('weight_decay', 0, 1, step=0.01)
    
    if model_name != 'linear':
        # Find number of layers
        n_layers = int(model_name[0])
        
        net_params = {
            'hl1_nodes': trial.suggest_int('hl1_nodes', 5, 500),
            'dropout': trial.suggest_float('dropout', 0, 1, step=0.01)
        }
        
        if n_layers > 2:
            net_params.update({'hl2_nodes': trial.suggest_int('hl2_nodes', 5, 500)})
        if n_layers > 3:
            net_params.update({'hl3_nodes': trial.suggest_int('hl3_nodes', 5, 500)})
        if n_layers > 4:
            net_params.update({'hl4_nodes': trial.suggest_int('hl4_nodes', 5, 500)})
        if n_layers > 5:
            net_params.update({'hl5_nodes': trial.suggest_int('hl5_nodes', 5, 500)})
    else:
        net_params = {}
        
    
    # Set cross validator
    cv = KFold(n_splits=5, shuffle=True, random_state=SEED)
    
    def fold_loss(train, folds, i, device='cpu'):
        """Calculate score within a single train/val fold"""
        
        # Set random seeds
        torch.manual_seed(SEED)
        np.random.seed(SEED)
        random.seed(SEED)
        
        # Train/Val split
        train_i, val_i = folds[i]
        train, val = train.iloc[train_i], train.iloc[val_i]
        
        #### PREPROCESSING ####
        
        # CatEncode train and val sets
        encoder = CatEncode(drop_dummy=True)
        encoder.fit(train)
        train = encoder.transform(train)
        val = encoder.transform(val)
        
        # Split into X and y sets
        X_train, y_train  = get_x_y(train, attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
        X_val, y_val  = get_x_y(val, attr_labels=['status', 'duration_m'], pos_label=True, survival=True)
        
        # Standardize X arrays
        ss = StandardScaler()
        X_train = ss.fit_transform(X_train).astype('float32')
        X_val = ss.transform(X_val).astype('float32')

        # Label Transforms
        num_durations = 100
        labtrans = MTLR.label_transform(num_durations, 'equidistant')
        get_target = lambda df: (df['duration_m'], df['status'])
        y_train_pycox = labtrans.fit_transform(*get_target(y_train))
        # y_val_pycox = labtrans.transform(*get_target(y_val))
        
        # Create neural net
        net = model_dict[model_name](
            in_features=X_train.shape[1],
            out_features=labtrans.out_features,
            **net_params
        )
        
        # Set optimizier
        optimizer = tt.optim.Adam(lr, weight_decay=weight_decay)
        
        # Set model
        model = MTLR(net, optimizer, duration_index=labtrans.cuts, device=device)
        
        # Train model
        log = model.fit(
            X_train,
            y_train_pycox,
            batch_size,
            epochs=epochs,
            verbose=False,
        )
        
        # compute survival function on val set
        surv = model.interpolate(10).predict_surv_df(X_val)
        
        def ibs(surv, y_train, y_test):
                
            surv = surv.T
            model_times = surv.columns
            
            # check max time in test is less than in train
            while y_test['duration_m'].max() > y_train['duration_m'].max():
                tmax_i = np.where(y_test['duration_m'] == y_test['duration_m'].max())[0]
                y_test = np.delete(y_test, tmax_i)
                surv = surv.drop(tmax_i)
            
            # Find indices of event times of model that correspond to test times
            indices = np.where(
                np.logical_and(
                    model_times > min(y_test['duration_m']),
                    model_times < max(y_test['duration_m'])
                )
            )
            
            # Set estimator time window to all available timepoints
            times = model_times[indices]
            
            # Subset surv to times
            surv = surv[times]
            
            return integrated_brier_score(
                y_train,
                y_test,
                surv,
                times
            )
            
        score = ibs(surv, y_train, y_val)
        
        # Report the score to pruner
        trial.report(score, i)
        
        # Prune if pruning
        if trial.should_prune():
            raise optuna.TrialPruned()
        
        return score
    
    # Get train/val indexes from cross validator
    folds = [(train_i, val_i) for train_i, val_i in cv.split(train)]
    
    cv_loss = [fold_loss(train, folds, i, device) for i in range(cv.get_n_splits())]
        
    return np.mean(cv_loss)

class OptunaSearch:
    
    def __init__(
        self, 
        mice_i:int, 
        model_name:str,
        device:str='cpu',
        verbosity=optuna.logging.CRITICAL
    ):
        """Start a hyperparameter search

        Args:
            mice_i (int): Imputed dataset to be used for tuning.
            model_name (str): Chosen model name (see model_dict from cjdsurv.models)
            device (str, optional): Train on 'cpu' or 'cuda. Defaults to 'cpu'
            verbosity (int, options): Optuna verbosity level. Defaults to optuna.logging.CRITICAL
        """
        
        self.mice_i = mice_i
        self.model_name = model_name
        self.seed = SEED
        self.device = device
        
        # Set Optuna verbosity
        optuna.logging.set_verbosity(verbosity)
        
        # Suppress Optuna ExperimentalWarning
        warnings.filterwarnings("ignore", category=ExperimentalWarning)
        
        # Connect to PostgreSQL database
        study_name = model_name
        database_name = f'mice_{mice_i}'
        storage_name = f'postgresql+psycopg2://postgres:optuna@localhost:5432/{database_name}'
        
        if not database_exists(storage_name):
            create_database(storage_name)
        
        # Create instance of a study
        self.study = optuna.create_study(
            study_name=study_name,
            storage=storage_name,
            sampler=optuna.samplers.TPESampler(seed=SEED),
            direction='minimize',
            pruner=optuna.pruners.MedianPruner(),
            load_if_exists=True
        )
        
    def run(self, imputed_data:list, n_trials:int=500):
        """Run searcher

        Args:
            imputed_data (list): list of imputed datasets
            n_trials (int, optional): Number of search trials. Defaults to 500.
        """
        
        # Run Hyperparameter Search
        self.study.optimize(
            lambda trial: objective(
                trial,
                imputed_data[self.mice_i],
                self.model_name,
                self.device,
            ),
            n_trials,
            show_progress_bar=True,
            catch=(ValueError,),
        )
        
        self.best_value = self.study.best_value
        self.best_params = self.study.best_params
        
        # # save parameters on completion
        # self.save_parameters()
        
        return self.best_params
        
    def save_parameters(self):
        
        result_dict = {
            'imputed_dataset': self.mice_i,
            'model': self.model_name,
            'best_params': self.study.best_params
        }
        try:
            mkdir('parameters')
        except:
            pass

        # Save best parameters to json
        try:
            with open(f'parameters/{self.model_name}.json', 'r') as file:
                result_json = json.load(file)
                
            result_json.append(result_dict)
            
            with open(f'parameters/{self.model_name}.json', 'w') as file:
                json.dump(result_json, file, indent=4)
        except:
            with open(f'parameters/{self.model_name}.json', 'w') as file:
                json.dump([result_dict], file, indent=4)
