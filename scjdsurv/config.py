# Set random seed for deterministic results
SEED = 0

# Fix batch size for mini batch gradient descent
BATCH_SIZE = 1000