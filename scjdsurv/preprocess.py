import pandas as pd
from sklearn.preprocessing import OneHotEncoder, OrdinalEncoder

__all__ = [
    'CatEncode',
]

class CatEncode:
    
    def __init__(self, drop_dummy=True):
        self.drop_dummy = drop_dummy
        return None
    
    def fit(self, data):
        
        # find categorical columns
        categorical_col = data.dtypes.loc[data.dtypes == 'category'].index.to_list()
        
        # set multivariable columns
        self.multivar_col = ['codon129', 'mriimp']
        
        # find binary columns
        self.binary_col = list(set(categorical_col).difference(self.multivar_col))
        
        # set columns to be left alone
        self.leave_cols = list(set(data.columns).difference(self.binary_col + self.multivar_col))
        
        # Fit one hot encoder
        self.ohe = OneHotEncoder(sparse_output=False)
        self.ohe.fit(data[self.multivar_col])
        
        # Fit ordinal encoder
        self.oe = OrdinalEncoder()
        self.oe.fit(data[self.binary_col])
    
    def transform(self, data):
        
        # Transform data
        ohe_data = self.ohe.transform(data[self.multivar_col])
        ohe_data = pd.DataFrame(ohe_data, columns=self.ohe.get_feature_names_out(), index=data.index)
        oe_data = self.oe.transform(data[self.binary_col])
        oe_data = pd.DataFrame(oe_data, columns=self.oe.get_feature_names_out(), index=data.index)
        
        result = pd.concat([data[self.leave_cols], ohe_data, oe_data], axis=1)
        
        # fill in a zeros column if missing
        expected_cols = [
            'codon129_MM', 'codon129_MV', 'codon129_VV',
            'mriimp_N', 'mriimp_P', 'mriimp_S',
            'sex', 'rtquic', 'csf1433', 'bg', 'thalamus', 'EEG', 'myoclonus', 'visual', 'cerebellar', 'pyramidal', 
            'expyramidal', 'akinetic_mutism', 'cognitive', 'psychiatric', 'behave'
        ]

        for col in expected_cols:
            if col not in result.columns:
                result[col] = 0
        
        # Drop dummy variables     
        if self.drop_dummy is True:
            result = result.drop(['codon129_VV', 'mriimp_S'], axis=1)
        
        # sort columns
        result = result.reindex(sorted(result.columns), axis=1)
        
        return result