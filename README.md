# sCJDSurv

This repository accompanies the paper [*Interpretable Deep Learning Survival Predictions in sporadic Creutzfeldt-Jakob Disease*, Tam et al 2024](https://link.springer.com/article/10.1007/s00415-024-12815-1).

## Getting Started

Clone this repository, and install required python dependencies using pip. It is recommended to do this in a virtual environment like conda or venv. I primarily used conda for this project with a python 3.11 environment:
```bash
# Create and activate a new conda environment with python=3.11
conda create -n scjdsurv python=3.11
conda activate scjdsurv

# Install dependencies using pip
pip install -r requirements.txt
```

## Content

1. **inference.ipynb** - Jupyter notebook with code for conducting inference on new data using pickled model ensemble and SHAP explainers
2. **training** - Directory containing Jupyter notebooks detailing every steps of model training and evaluation

## Dataset
For the purpose of patient confidentiality in this rare diagnosis, the original dataset and pickled models are not publicly available. Access may be granted following a formal written request to the authors. Please see the full paper for further details.