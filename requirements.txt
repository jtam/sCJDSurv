# General
jupyter
miceforest
numpy
openpyxl
optuna
pandas
seaborn
sqlalchemy_utils
tabulate
tqdm
shap

# scikit-learn
scikit-learn
scikit-learn-intelex
scikit-survival
sklearn-pandas

# lifelines
lifelines

# pycox
pycox
torchtuples

# PyTorch (CPU)
--extra-index-url https://download.pytorch.org/whl/cpu
torch
torchaudio
torchvision